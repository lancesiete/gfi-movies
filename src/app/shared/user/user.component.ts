import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userSession: string;
  storageCount: number;

  constructor(private router: Router) {
    this.userSession = JSON.parse(sessionStorage.getItem("admin"));
  }

  ngOnInit() {
    this.itemsLocalStorage();
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  favorite() {
    this.router.navigate(['/backend/favorite']);
  }

  itemsLocalStorage() {
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    this.storageCount = favorites.length;
  }

}