import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [ UserComponent, SearchComponent ],
  declarations: [ UserComponent, SearchComponent ],
  providers: []
})

export class SharedModule { }
