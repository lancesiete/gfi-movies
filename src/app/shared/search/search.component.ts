import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

   @Output() movie = new EventEmitter();
  constructor(private router: Router) { }

  ngOnInit() {
  }

  getSearch(search: string) {
    this.movie.emit(search);
    this.router.navigate(['/backend/movies', search]);
  }

}