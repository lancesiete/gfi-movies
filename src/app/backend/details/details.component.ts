import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../../interfaces/movie';
import { GetMoviesService } from '../../services/get-movies.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  movie: Movie;
  constructor(private route: ActivatedRoute, private _getMovies: GetMoviesService) { }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this._getMovies.getMovie(param.id).subscribe((data: Movie) => {
        console.log(data);
        this.movie = data;
      });
    });
  }

}