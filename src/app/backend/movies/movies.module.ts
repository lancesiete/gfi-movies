import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { SharedModule } from '../../shared/shared.module';



@NgModule({
  imports: [ CommonModule, MoviesRoutingModule, SharedModule ],
  exports: [],
  declarations: [ MoviesComponent ],
  providers: []
})

export class MoviesModule {}