import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GetMoviesService } from '../../services/get-movies.service';
import { Movie } from '../../interfaces/movie';
import { UserComponent } from '../../shared/user/user.component';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit, OnDestroy {
  movies$: Movie;
  page: number = 1;
  @ViewChild( UserComponent, {static: false} ) countFavorite: UserComponent;
  constructor(private _getMovies: GetMoviesService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.searchMovie(param.movie);
    });
  }

  selectMovie( movie: Movie ) {
    this.router.navigate(['/backend/details', movie.imdbID]);
  }

  searchMovie(movie: string) {
    this._getMovies.getMovies(movie).subscribe(data => {
      this.movies$ = data;
    });
  }

  setFavorites(movie: Movie) {
    const favorites = [];
    if (localStorage['favorites']) {
      const recover = JSON.parse(localStorage.getItem('favorites'));
      recover.push(movie);
      localStorage.setItem('favorites', JSON.stringify(recover));
    } else {
      favorites.push(movie);
      localStorage.setItem('favorites', JSON.stringify(favorites));
    }
    this.countFavorite.itemsLocalStorage();
  }

  ngOnDestroy() {

  }

}
