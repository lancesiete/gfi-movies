
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Movie } from '../../interfaces/movie';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})
export class FavoriteComponent implements OnInit {
  
  movies: Movie[];

  constructor(private router: Router) { }

  ngOnInit() {
    const favorite = JSON.parse(localStorage.getItem('favorites'));
    this.movies = favorite;
  }

  selectMovie( movie: Movie ) {
    this.router.navigate(['/backend/details', movie.imdbID]);
  }

}
