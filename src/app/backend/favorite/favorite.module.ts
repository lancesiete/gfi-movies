import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoriteComponent } from './favorite.component';
import { FavoriteRoutingModule } from './favorite-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [ CommonModule, FavoriteRoutingModule, SharedModule ],
  exports: [],
  declarations: [ FavoriteComponent ],
  providers: []
})

export class FavoriteModule {}