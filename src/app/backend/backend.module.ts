import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackendRoutingModule } from './backend-routing.module';

@NgModule({
  imports: [ 
    CommonModule, 
    BackendRoutingModule
  ],
  exports: [],
  declarations: [],
  providers: []
})

export class BackendModule {}
