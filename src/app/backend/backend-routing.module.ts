import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../services/auth-guard.service';

const routes: Routes = [
  {
      path: 'movies',
      canActivate: [AuthGuardService],
      loadChildren: './movies/movies.module#MoviesModule'
  },
  {
      path: 'details/:id',
      canActivate: [AuthGuardService],
      loadChildren: './details/details.module#DetailsModule'
  },
  {
      path: 'favorite',
      canActivate: [AuthGuardService],
      loadChildren: './favorite/favorite.module#FavoriteModule'
  }
]

@NgModule({
  imports: [ 
    RouterModule.forChild(routes)
  ],
  exports: [ 
    RouterModule 
  ]
})

export class BackendRoutingModule {}