import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Movie } from '../interfaces/movie';
import { Observable } from 'rxjs';

@Injectable()
export class GetMoviesService {

  private apiKey = 'f12ba140';

  movies: Movie[];

  constructor( private http: HttpClient ) { }

  getMovies( movie: string ): Observable<Movie> {
    return this.http.get(`http://www.omdbapi.com/?apikey=${this.apiKey}&s=${movie}&plot=full`)
      .pipe(
        map( data => data['Search'] )
      );
  }

  getMovie(id: string) {
    return this.http.get(`http://www.omdbapi.com/?apikey=${this.apiKey}&i=${id}&plot=full`)
      .pipe(
        map( data => data )
      );
  }

}