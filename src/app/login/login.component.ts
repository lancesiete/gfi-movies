import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    user: new FormControl(''),
    pass: new FormControl('')
  });

  constructor(private router: Router) { }

  ngOnInit() {
  }

  login(){
    const userData = this.loginForm.value;
    if(userData.user == 'admin' && userData.pass == 'admin' ) {
      sessionStorage.setItem("admin", JSON.stringify(userData.user));
      this.router.navigate(['/backend/movies/', '']);
    }
  }

}